from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse

def redir(request):
    return redirect(reverse('intraAsso:connexion'))

def accueil(request):
    if not request.user.is_authenticated:
        return redirect(reverse('intraAsso:connexion'))
    return redirect(reverse('intraAsso:accueil'))

def redir2(request):
    return redirect(reverse('intraAsso:logged'))
