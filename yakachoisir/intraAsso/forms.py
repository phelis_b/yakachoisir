from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.db import models
from .models import User, Member, Association, Creation_association

class UserIsPresent(forms.Form):
    email = forms.EmailField(label="email", max_length=64, required=True, widget=forms.TextInput(attrs={'placeholder':'email'}))
    password = forms.CharField(label="password", required=True, widget=forms.PasswordInput(attrs={'placeholder':'password'}))

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=64, required=True)
    last_name = forms.CharField(max_length=64, required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2', )

class Creation_Assos(ModelForm):
    class Meta:
        model = Creation_association
        fields = ('name', 'mail', 'site', 'description', 'President', 'Vice_president', 'Secretaire' , 'Trésorier', 'Association_tutrice', 'motivation', 'contract', )

class Postulat(forms.Form):
    motivation = forms.FileField(label="Motivation",  required=False,  widget=forms.FileInput(attrs={'accept':'application/pdf'}))

class Edit_Asso(forms.Form):
    name = forms.CharField(label="Nom", max_length=64, required=False)
    mail = forms.EmailField(label="Mail", max_length=64,  required=False)
    site = forms.CharField(label="Site internet", max_length=64, required=False)
    description = forms.CharField(label="Description",  required=False, widget=forms.Textarea)
    motivation = forms.BooleanField(label="Motivation",  required=False)
    contrat = forms.FileField(label="Contrat",  required=False, widget=forms.FileInput(attrs={'accept':'application/pdf'}))


class Change_Password(UserCreationForm):
    class Meta:
        model = User
        fields = ('password1', 'password2', )

class Create_actu(forms.Form):
    titre = forms.CharField(label="titre", max_length=128, required=True)
    description = forms.CharField(label="Description",  required=True, widget=forms.Textarea)

class Create_comment(forms.Form):
    comment = forms.CharField(label="Commentaire",  required=True, widget=forms.Textarea)
