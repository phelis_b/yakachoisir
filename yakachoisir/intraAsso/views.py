from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.utils import timezone
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.syndication.views import Feed
from django.contrib.postgres.search import SearchVector
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings
from django.forms.models import modelformset_factory
from django.db.models import Q

from .resources import AssoRessource, MemberRessource
from .forms import UserIsPresent, SignUpForm, Creation_Assos, Postulat, Edit_Asso, Change_Password, Create_actu, Create_comment
from .models import User, Association, Rss_files, Status, Creation_association, Member, Tuteur, Role, Postulant, Enacs, Transaction, Budget_request, Reaction, Liked, Rss_subscription

################################################################################

def change_bureau(request, name):
    association = Association.objects.get(id=name)
    members = association.get_members()
    president = association.get_president()
    if request.method == 'GET':
        if (request.user.is_staff):
            if (request.GET.get('Pr') and request.GET.get('Vp') and request.GET.get('Se') and request.GET.get('Tr')):
                return edit_res_bureau(request, name)
        else:
            if (request.GET.get('Vp') and request.GET.get('Se') and request.GET.get('Tr')):
                return edit_bureau(request, name)
    role = Role.objects.get(role_str="None")
    nobody = Member.objects.filter(association=association, role=role)
    post = Postulant.objects.filter(association=association)
    al = Member.objects.filter(association=association)
    context = { "association" : association, "president" : president, "edit" : 1,
                    "members" : members, "nobody" : nobody, "postulants" : post, "all" : al}
    if association.club:
        tut = Tuteur.objects.get(son=association.id).mother
        context["tuteur"]=tut
    return render(request, "intraAsso/assos_president.html", context)

################################################################################

def signup(request):
    if request.user.is_authenticated:
        return redirect(reverse('intraAsso:accueil'))
    if request.method == 'POST':
        form = SignUpForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            form.save()
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            email(
                "Incription intranet Epita",
                "Votre inscription à l'intranet des associations d'EPITA est terminé.",
                [username, ]
            )
            return redirect(reverse('intraAsso:accueil'))
    else:
        form = SignUpForm(request.POST or None)
    return render(request, "intraAsso/inscription.html", {"form" : form})

################################################################################

def connexion(request):
    if request.user.is_authenticated:
        return redirect(reverse('intraAsso:accueil'))
    if request.method == 'POST':
        form = UserIsPresent(request.POST or None)
        if form.is_valid():
            mail = form.cleaned_data.get("email")
            mdp = form.cleaned_data.get("password")
            user = authenticate(username=mail, password=mdp)
            if user is None:
                return render(request, "intraAsso/connexion.html", {"form" : form})
            login(request, user)
            return redirect(reverse('intraAsso:accueil'))
    else:
        form = UserIsPresent(request.POST or None)
    return render(request, "intraAsso/connexion.html", {"form" : form})

################################################################################

def create_asso(request):
    if not request.user.is_authenticated:
        return redirect(reverse('intraAsso:connexion'))
    if request.method == 'POST':
        form = Creation_Assos(request.POST, request.FILES)
        if form.is_valid():
            Contract = form.cleaned_data.get('contract')
            creat = form.save(commit=False)
            list = []
            list.append(creat.President.id)
            list.append(creat.Vice_president.id)
            list.append(creat.Secretaire.id)
            list.append(creat.Trésorier.id)
            if (not check_double(list)):
                return render(request, "intraAsso/creat_asso.html", {"form" : form})
            name = creat.name
            try:
                asso = get_object_or_404(Association, name=name)
                return render(request, "intraAsso/creat_asso.html", {"form" : form})
            except:
                pass
            creat.user = request.user
            status = Status.objects.get(status="Reçu")
            creat.status = status
            creat.contract = Contract
            creat.save()
            email(
                "Creation association",
                "Votre demande de création de l'association " + name + " a été reçue.",
                [request.user.email, ]
            )
            return redirect(reverse('intraAsso:accueil'))
    else:
        form = Creation_Assos(request.POST, request.FILES)
    return render(request, "intraAsso/creat_asso.html", {"form" : form})

################################################################################

class AssociationView(generic.ListView):
    template_name = 'intraAsso/assos_list.html'
    context_object_name = 'liste'
    queryset = Association.objects.all().order_by("name")

################################################################################

# quick and dirty

def associationView(request):
    if request.user.is_authenticated:
        return AssociationView.as_view()(request)
    return redirect(reverse('intraAsso:connexion'))

################################################################################

def redir(request):
    if request.user.is_authenticated:
        return redirect(reverse('intraAsso:accueil'))
    return redirect(reverse('intraAsso:connexion'))

################################################################################

def logoutView(request):
    if request.user.is_authenticated:
        logout(request)
        return render(request, "intraAsso/logout.html")
    return redirect(reverse('intraAsso:connexion'))

################################################################################ ################check this fct !!!!

def detail(request, name):
    ##### Pärtie 1 #####
    association = Association.objects.get(id=name)
    form = Postulat(request.POST, request.FILES)
    members = association.get_members()
    president = association.get_president()
    enacs = Enacs.objects.all()[0]
    abonne = 0
    try:
        user = request.user
        p =  Rss_subscription.objects.get(user=user, association=association)
        abonne = 1
    except:
        abonne = 0
    ##### Pärtie 2 #####
    if request.method == 'POST':
        if form.is_valid():
            if association.motivation:
                Motivation = form.cleaned_data.get("motivation")
                if Motivation is not None:
                    try:
                        pos = Postulant.objects.get(user=request.user, association=association)
                    except:
                        p = Postulant(user=request.user,
                                      association=association,
                                      motiv_file=Motivation,
                                      status=Status.objects.get(status="Reçu"))
                        p.save()
                        email = EmailMessage(
                            "Demande d'admission",
                            "L'utilisateur " + p.user.__str__() + " souhaite intéger votre association " + p.association.name + ".",
                            'intranetepita@gmail.com',
                            [president.user.email,],
                        )
                        email.attach_file(p.motiv_file.name)
                        email.send()
    ##### Pärtie 3 #####
    context = { "association" : association, "president" : president,
                "members" : members, "form" : form, "enacs" : enacs}
    context["abonne"]=abonne
    if association.club:
        tut = Tuteur.objects.get(son=association.id).mother
        context["tuteur"]=tut
    try:
        user = get_object_or_404(Member, user=request.user, association=association)
    except:
        if (not request.user.is_staff):
            ke = 1
            try:
                Postulant.objects.get(user=request.user, association=association)
            except:
                ke = 0
            context["k"]=ke
            return render(request, "intraAsso/profil_assos.html", context)
    ##### Pärtie 4 #####
    role = Role.objects.get(role_str="None")
    nobody = Member.objects.filter(association=association, role=role)
    post = Postulant.objects.filter(association=association)
    context = { "association" : association, "president" : president, "edit" : 0,
                    "members" : members, "nobody" : nobody, "postulants" : post, "enacs" : enacs}
    context["abonne"]=abonne
    if association.club:
        tut = Tuteur.objects.get(son=association.id).mother
        context["tuteur"]=tut
    if (request.user.is_staff or user.role.role_str == "President"):
        return render(request, "intraAsso/assos_president.html", context)
    if (user.role.role_str == "Vice-President"):
        return render(request, "intraAsso/assos_vice_president.html", context)
    if (user.role.role_str == "Tresorier"):
        return render(request, "intraAsso/assos_tresorier.html", context)
    if (user.role.role_str == "Secretaire"):
        return render(request, "intraAsso/assos_secretaire.html", context)
    if (user.role.role_str == "None"):
        return render(request, "intraAsso/assos_nobody.html", context)

################################################################################

def accueil(request):
    if not request.user.is_authenticated:
        return redirect(reverse('intraAsso:connexion'))
    if request.user.is_staff:
        s = Status.objects.get(status="Reçu")
        s2 = Status.objects.get(status="En attente de validation")
        creat = Creation_association.objects.filter(status=s)
        for l in creat:
            l.status = s2
            email(
                "Creation association",
                "Votre demande de création de l'association " + l.name + " est en cours de validation.",
                [l.user.email, ]
            )
            l.save()
        enacs = Enacs.objects.all()[0]
        liste = []
        creat2 = Creation_association.objects.filter(status=s2)
        for k in creat2:
            liste.append(k)
        return render(request, "intraAsso/accueil_responsable.html", {"liste" : liste,
                                                                      "enacs" : enacs})
    creat = Creation_association.objects.filter(user=request.user)
    mem = Member.objects.filter(user=request.user)
    assos = []
    for l in mem:
        assos.append(l.association)
    r = Rss_subscription.objects.filter(user=request.user)
    actus = []
    for a in r:
        rss = Rss_files.objects.filter(association=a.association)
        for b in rss:
            actus.append(b)
    return render(request, "intraAsso/acceuil.html", {"liste" : creat ,
                                                      "list_asso" : assos,
                                                      "actu" : actus})

################################################################################

@login_required
def logged(request):
    user = request.user
    firstname = user.first_name
    lastname = user.last_name
    email = user.email
    try:
        u = get_object_or_404(User, email=email)
        u.first_name = firstname
        u.last_name = lastname
        u.cri = True
        u.save()
    except User.DoesNotExist:
        u = UserManager.create_cri_user(firstname, lastname, email)
        u.save()
    return redirect(reverse('intraAsso:accueil'))

################################################################################

def demand_asso(request, name):
    creat = Creation_association.objects.get(id=name)
    File = creat.contract.name.split("/")[-1]
    if not request.user.is_staff:
        return render(request, "intraAsso/demande_asso.html", {"asso" : creat, "file" : File})
    return render(request, "intraAsso/validate_asso.html", {"asso" : creat, "file" : File})

################################################################################

def research(request):
    query = request.GET.get('q')
    if (query):
        Assos = Association.objects.filter(name__icontains=query).order_by("name")
        users = User.objects.annotate(
            search=SearchVector('first_name', 'last_name'),).filter(search__icontains=query).order_by("last_name")
        list = []
        for i in users:
            m = Member.objects.filter(user=i)
            for k in m:
                if k.role.role_str != "None":
                    list.append(i)
                    break
        return render(request, "intraAsso/search.html", {"asso" : Assos,
                                                         "user" : list})
    users = User.objects.all().order_by("last_name")
    Assos = Association.objects.all().order_by("name")
    list = []
    for i in users:
        m = Member.objects.filter(user=i)
        for k in m:
            if k.role.role_str != "None":
                list.append(i)
                break
    return render(request, "intraAsso/search.html", {"asso" : Assos,
                                                     "user" : list})

################################################################################


def validation_asso(request, name):
    creat = Creation_association.objects.get(id=name)
    Asso = Association(name=creat.name,
                       mail=creat.mail,
                       site=creat.site,
                       description=creat.description,
                       motivation=creat.motivation,
                       contract=creat.contract,
                       club=True)
    Asso.save()
    Tut = Tuteur(mother=creat.Association_tutrice,
                 son=Asso.id)
    Tut.save()
    ##############
    mem = Member(user=creat.President,
                 association=Asso,
                 role=Role.objects.get(role_str="President"),
                 enacs=0)
    mem.save()
    ##############
    mem = Member(user=creat.Vice_president,
                 association=Asso,
                 role=Role.objects.get(role_str="Vice-President"),
                 enacs=0)
    mem.save()
    ##############
    mem = Member(user=creat.Secretaire,
                 association=Asso,
                 role=Role.objects.get(role_str="Secretaire"),
                 enacs=0)
    mem.save()
    ##############
    mem = Member(user=creat.Trésorier,
                 association=Asso,
                 role=Role.objects.get(role_str="Tresorier"),
                 enacs=0)
    mem.save()
    ##############
    email(
        "Creation association",
        "Votre demande de création de l'association " + Asso.name + " a été validée.",
        [creat.user.email, ]
    )
    creat.delete()
    return redirect(reverse('intraAsso:accueil'))

################################################################################

def refu_asso(request, name):
    creat = Creation_association.objects.get(id=name)
    email(
        "Creation association",
        "Votre demande de création de l'association " + creat.name + " a été refusée.",
        [creat.user.email, ]
    )
    creat.delete()
    return redirect(reverse('intraAsso:accueil'))

################################################################################ << A partir de la Jeremie >>

def show_user(request, name):
    user = User.objects.get(id=name)
    mem = Member.objects.filter(user=user)
    enac = 0
    for l in mem:
        enac += l.enacs
    postule = Postulant.objects.filter(user=user)
    return render(request, "intraAsso/show_user.html", {"list" : mem,
                                                        "post" : postule,
                                                        "user" : user,
                                                        "enacs" : enac})

################################################################################

def create_post(request, name):
    association = Association.objects.get(id=name)
    try:
        Postulant.objects.get(user=request.user, association=association)
    except:
        p = Postulant(user=request.user,
                      association=association,
                      status=Status.objects.get(status="Reçu"))
        p.save()
        pre = p.association.get_president()
        email(
            "Demande d'admission",
            "L'utilisateur " + p.user.__str__ + " souhaite intéger votre association " + p.association.name + ".",
            [creat.user.email, ]
        )
    return HttpResponseRedirect(reverse_lazy('intraAsso:detail', args=(association.id,)))

################################################################################

def validation_post(request, name):
    post = Postulant.objects.get(id=name)
    association = post.association
    m = Member(user=post.user,
               association=association,
               role=Role.objects.get(role_str="None"),
               enacs=0)
    m.save()
    email(
        "Demande d'admission",
        "Vous avez été accépté dans l'association " + association.name + ".",
        [m.user.email, ]
    )
    post.delete()
    return HttpResponseRedirect(reverse_lazy('intraAsso:detail', args=(association.id,)))

################################################################################

def refu_post(request, name):
    post = Postulant.objects.get(id=name)
    association = post.association
    post.delete()
    email(
        "Demande d'admission",
        "Le bureau de l'association " + association.name + " a refusé de vous admettre dans l'association.",
        [post.user.email, ]
    )
    return HttpResponseRedirect(reverse_lazy('intraAsso:detail', args=(association.id,)))

################################################################################

def kick(request, name):
    m = Member.objects.get(id=name)
    user = m.user
    association = m.association
    m.delete()
    email(
        "Expulsion",
        "Vous avez été expulsé de l'association " + association.name + " .",
        [user.email, ]
    )
    return HttpResponseRedirect(reverse_lazy('intraAsso:detail', args=(association.id,)))

################################################################################

def enacs(request):
    enacs = Enacs.objects.all()[0]
    if (enacs.enabled):
        enacs.enabled = 0
    else:
        enacs.enabled = 1
    enacs.save()
    asso = Association.objects.all()
    lis = []
    for a in asso:
        p = a.get_president()
        lis.append(p.user.email)
    if (enacs.enabled):
        for a in asso:
            p = a.get_president()
            email(
                "Points Enacs",
                "Le responsable des associations a ouvert l'attribution des points enacs.",
                lis
            )
    else:
        for a in asso:
            p = a.get_president()
            email(
                "Points Enacs",
                "Le responsable des associations a fermé l'attribution des points enacs.",
                lis
            )
    return redirect(reverse('intraAsso:accueil'))

################################################################################

def edit_bureau(request, name):
    asso = Association.objects.get(id=name)
    role_none = Role.objects.get(role_str="None")
    vice = request.GET.get('Vp')
    secr = request.GET.get('Se')
    tres = request.GET.get('Tr')
    liste = []
    if (vice):
        liste.append(vice)
    if (secr):
        liste.append(secr)
    if (tres):
        liste.append(tres)
    if (not check_double(liste)):
        return HttpResponseRedirect(reverse('intraAsso:change_bureau', args=(asso.id,)))
    ####### Vice-President
    if (vice):
        role_vice = Role.objects.get(role_str="Vice-President")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_vice)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        vice2 = vice.split()
        user_vice = User.objects.get(first_name=vice2[0], last_name=vice2[1])
        vice_mem = Member.objects.get(user=user_vice, association=asso)
        vice_mem.role = role_vice
        vice_mem.save()
    ####### Secretaire
    if (secr):
        role_secr = Role.objects.get(role_str="Secretaire")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_secr)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        secr2 = secr.split()
        user_secr = User.objects.get(first_name=secr2[0], last_name=secr2[1])
        secr_mem = Member.objects.get(user=user_secr, association=asso)
        secr_mem.role = role_secr
        secr_mem.save()
    ####### Tresorier
    if (tres):
        role_tres = Role.objects.get(role_str="Tresorier")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_tres)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        tres2 = tres.split()
        user_tres = User.objects.get(first_name=tres2[0], last_name=tres2[1])
        tres_mem = Member.objects.get(user=user_tres, association=asso)
        tres_mem.role = role_tres
        tres_mem.save()
    return HttpResponseRedirect(reverse('intraAsso:detail', args=(asso.id,)))

################################################################################

def edit_res_bureau(request, name):
    asso = Association.objects.get(id=name)
    role_none = Role.objects.get(role_str="None")
    pres = request.GET.get('Pr')
    vice = request.GET.get('Vp')
    secr = request.GET.get('Se')
    tres = request.GET.get('Tr')
    liste = []
    if (pres):
        liste.append(pres)
    if (vice):
        liste.append(vice)
    if (secr):
        liste.append(secr)
    if (tres):
        liste.append(tres)
    if (not check_double(liste)):
        return HttpResponseRedirect(reverse('intraAsso:change_bureau', args=(asso.id,)))
    ####### President
    if (pres):
        role_pres = Role.objects.get(role_str="President")
        try:
            vrai_pres = Member.objects.get(association=asso, role=role_pres)
            vrai_pres.role = role_none
            vrai_pres.save()
        except:
            pass
        pres2 = pres.split()
        user_pres = User.objects.get(first_name=pres2[0], last_name=pres2[1])
        pres_mem = Member.objects.get(user=user_pres, association=asso)
        pres_mem.role = role_pres
        pres_mem.save()
    ####### Vice President
    if (vice):
        role_vice = Role.objects.get(role_str="Vice-President")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_vice)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        vice2 = vice.split()
        user_vice = User.objects.get(first_name=vice2[0], last_name=vice2[1])
        vice_mem = Member.objects.get(user=user_vice, association=asso)
        vice_mem.role = role_vice
        vice_mem.save()
    ####### Secretaire
    if (secr):
        role_secr = Role.objects.get(role_str="Secretaire")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_secr)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        secr2 = secr.split()
        user_secr = User.objects.get(first_name=secr2[0], last_name=secr2[1])
        secr_mem = Member.objects.get(user=user_secr, association=asso)
        secr_mem.role = role_secr
        secr_mem.save()
    ####### Tresorier
    if (tres):
        role_tres = Role.objects.get(role_str="Tresorier")
        try:
            vrai_vice = Member.objects.get(association=asso, role=role_tres)
            vrai_vice.role = role_none
            vrai_vice.save()
        except:
            pass
        tres2 = tres.split()
        user_tres = User.objects.get(first_name=tres2[0], last_name=tres2[1])
        tres_mem = Member.objects.get(user=user_tres, association=asso)
        tres_mem.role = role_tres
        tres_mem.save()
    return HttpResponseRedirect(reverse('intraAsso:detail', args=(asso.id,)))

################################################################################

def check_double(l):
    for i in range (0, len(l)):
        for k in range (0, len(l)):
            if (k == i):
                continue
            if (l[i] == l[k]):
                return False
    return True

################################################################################

def edit_asso(request, name):
    if not request.user.is_authenticated:
        return redirect(reverse('intraAsso:connexion'))
    asso = Association.objects.get(id=name)
    context = {"name" : asso.name,
               "mail" : asso.mail,
               "site" : asso.site,
               "description" : asso.description,
               "motivation" : asso.motivation,
               "contrat" : asso.contract}
    if request.method == 'POST':
        form = Edit_Asso(request.POST, request.FILES, initial=context)
        if form.is_valid():
            Name = form.cleaned_data.get("name")
            Mail = form.cleaned_data.get("mail")
            Site = form.cleaned_data.get("site")
            Description = form.cleaned_data.get("description")
            Motivation = form.cleaned_data.get("motivation")
            Clear = form.cleaned_data.get("contrat")
            Contract = None
            if (request.FILES):
                Contract = request.FILES["contrat"]
            try:
                A = Association.objects.get(name=Name)
                if asso.name == A.name:
                    raise Exception
                return render(request, "intraAsso/edit_asso.html", {"form" : form})
            except:
                asso.name = Name
                asso.mail = Mail
                asso.site = Site
                asso.description = Description
                asso.motivation = Motivation
                if (Clear is False):
                    asso.contract = None
                if (Contract):
                    asso.contract = Contract
                asso.save()
            return redirect(reverse('intraAsso:detail', args=(asso.id,)))
    else:
        form = Edit_Asso(initial=context)
    return render(request, "intraAsso/edit_asso.html", {"form" : form})

################################################################################

def email(subject, message, recipient_list):
    oui = 'intranetepita@gmail.com'
    send_mail(subject, message, oui, recipient_list, fail_silently=False)
    return None

################################################################################

def edit_enacs(request, name):
    enacs = Enacs.objects.all()[0]
    Asso = Association.objects.get(id=name)
    if (not enacs.enabled):
        return HttpResponseRedirect(reverse('intraAsso:detail', args=(name,)))
    if (not request.user.is_staff):
        try:
            user = get_object_or_404(Member, user=request.user, association=Asso)
        except:
            return HttpResponseRedirect(reverse('intraAsso:detail', args=(name,)))
    members = Asso.get_members()
    president = Asso.get_president()
    role = Role.objects.get(role_str="None")
    role_pres = Role.objects.get(role_str="President")
    query = Member.objects.filter(association=Asso).order_by('role')


    ProductFormSet = modelformset_factory(Member, fields=('enacs',), exclude=('id',), extra=0)
    if (request.user.is_staff):
        pass
    elif (user.role.role_str == "President"):
        query = Member.objects.filter(association=Asso).exclude(role=role_pres).order_by('role')
    else:
        query = Member.objects.filter(association=Asso, role=role).order_by('role')
    l = list(query)

    if request.method == 'POST':
        formset = ProductFormSet(request.POST, queryset=query)
        if (formset.is_valid()):
            formset.save()
        return HttpResponseRedirect(reverse('intraAsso:detail', args=(name,)))

    formset = ProductFormSet(request.POST or None, queryset=query)
    return render(request, 'intraAsso/give_enacs.html', {'formset': formset,
                                                         "list" : l})


################################################################################

def export_asso(request):
    query = Association.objects.all().order_by('name')
    person_resource = AssoRessource()
    dataset = person_resource.export(query)
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="associations.xls"'
    return response

################################################################################

def export_bureau(request):
    role = Role.objects.get(role_str="None")
    query = Member.objects.all().exclude(role=role).order_by('association')
    person_resource = MemberRessource()
    dataset = person_resource.export(query)
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="members.xls"'
    return response

################################################################################

def export_enacs(request):
    query = Member.objects.all().order_by('association')
    person_resource = MemberRessource()
    dataset = person_resource.export(query)
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="enacs.xls"'
    return response

###############################################################################

def edit_password(request):
    form = Change_Password(request.POST or  None)
    if request.method == 'POST':
        if form.is_valid():
            raw = form.cleaned_data.get('password1')
            user = request.user
            user.set_password(raw)
            user.save()
        return redirect(reverse('intraAsso:accueil'))
    return render(request, 'intraAsso/change_mypass.html', {'form': form})

###############################################################################

def detail_budget(request, name):
    asso = Association.objects.get(id=name)
    b = Budget_request.objects.filter(association=asso)
    t = Transaction.objects.filter(association=asso)
    return render(request, 'intraAsso/budget.html', {'asso': asso,
                                                     "budget" : b,
                                                     "transaction" : t})

###############################################################################

def actu(request, name):
    rss = Rss_files.objects.get(id=name)
    reac = Reaction.objects.filter(reaction=rss).order_by("-id")
    like = Liked.objects.filter(reaction=rss).count()
    return render(request, 'intraAsso/actu.html', {'rss': rss,
                                                   'reac' : reac,
                                                   'like' : like})

###############################################################################

def create_actu(request, name):
    asso = Association.objects.get(id=name)
    form = Create_actu(request.POST or  None)
    if request.method == 'POST':
        if form.is_valid():
            Title = form.cleaned_data.get('titre')
            description = form.cleaned_data.get('description')
            r = Rss_files(association=asso, title=Title, description=description)
            r.save()
            return HttpResponseRedirect(reverse('intraAsso:detail', args=(name,)))
    return render(request, 'intraAsso/create_actu.html', {'form': form})

###############################################################################

def create_comment(request, name):
    r = Rss_files.objects.get(id=name)
    form = Create_comment(request.POST or  None)
    if request.method == 'POST':
        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            r = Reaction(user=request.user, reaction=r, commentaire=comment)
            r.save()
            return HttpResponseRedirect(reverse('intraAsso:actu', args=(name,)))
    return render(request, 'intraAsso/create_comment.html', {'form': form})

###############################################################################

def like(request, name):
    r = Rss_files.objects.get(id=name)
    user = request.user
    try:
        l = Liked.objects.get(user=user, reaction=r)
        l.delete()
    except:
        l = Liked(user=user, reaction=r)
        l.save()
    return HttpResponseRedirect(reverse('intraAsso:actu', args=(name,)))

###############################################################################

def edit_comment(request, name):
    r = Reaction.objects.get(id=name)
    context = {'comment' : r.commentaire}

    if request.method == 'POST':
        form = Create_comment(request.POST, initial=context)
        if form.is_valid():
            comment = form.cleaned_data.get("comment")
            r.commentaire = comment
            r.save()
            return redirect(reverse('intraAsso:actu', args=(r.reaction.id,)))
    else:
        form = Create_comment(initial=context)
    return render(request, "intraAsso/create_comment.html", {"form" : form})

###############################################################################

def abonne(request, name):
    asso = Association.objects.get(id=name)
    user = request.user
    try:
        p = Rss_subscription.objects.get(user=user, association=asso)
        p.delete()
    except:
        p = Rss_subscription(user=user, association=asso)
        p.save()
    return HttpResponseRedirect(reverse('intraAsso:detail', args=(name,)))

###############################################################################
