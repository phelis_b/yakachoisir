from import_export import resources
from .models import Association, Member

class AssoRessource(resources.ModelResource):
    class Meta:
        model = Association
        exclude = ('contract', 'id')

class MemberRessource(resources.ModelResource):
    class Meta:
        model = Member
        exclude = ('id')
