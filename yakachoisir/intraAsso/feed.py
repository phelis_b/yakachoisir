from django.contrib.syndication.views import Feed
from django.urls import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Rss_files

################################################################################

class Actualites(Feed):
    title = "Actualités"
    link = "IntraAsso/Rss/"
    description = "Actualités des associations d'EPITA"

    def items(self):
        return Rss_files.objects.all().order_by('-id')

    def item_title(self, item):
        return item.association.name + " : " + item.title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return reverse('intraAsso:actu', args = [item.id])

################################################################################
