from django.contrib import admin

from .models import User, Association, Role, File_type, Imported_files, Enacs_form, Status, Member, Transaction, Reunion, Enacs, Budget_request, Rss_files, Reaction, Postulant, Creation_association, Tuteur

admin.site.register(User)
admin.site.register(Association)
admin.site.register(Role)
admin.site.register(File_type)
admin.site.register(Imported_files)
admin.site.register(Enacs_form)
admin.site.register(Status)
admin.site.register(Member)
admin.site.register(Transaction)
admin.site.register(Reunion)
admin.site.register(Enacs)
admin.site.register(Budget_request)
admin.site.register(Rss_files)
admin.site.register(Reaction)
admin.site.register(Postulant)
admin.site.register(Creation_association)
admin.site.register(Tuteur)
