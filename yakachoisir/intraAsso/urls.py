from django.urls import path
from django.conf.urls import url
from . import views
from . import feed

app_name = 'intraAsso'
urlpatterns = [
    path('home/', views.accueil, name='accueil'),
    path('connexion/', views.connexion, name='connexion'),
    path('inscription/', views.signup, name='inscription'),
    path('associations/', views.associationView, name='associations'),
    path("associations/<int:name>/", views.detail, name="detail"),
    path("postule/<int:name>/", views.create_post, name="create_post"),
    path('logout/', views.logoutView, name='logout'),
    path('', views.redir, name='redir'),
    path('logged/', views.logged, name='logged'),
    path('enacs/', views.enacs, name='enacs'),
    path('enacs/<int:name>/', views.edit_enacs, name='edit_enacs'),

    path('change_bureau/<int:name>/', views.change_bureau, name='change_bureau'),
    path('edit_asso/<int:name>/', views.edit_asso, name='edit_asso'),

    path('Rss/', feed.Actualites(), name='actualite'),
    path('Actualités/<int:name>/', views.actu, name='actu'),
    path('Création_actualité/<int:name>/', views.create_actu, name='create_actu'),
    path('Création_commentaire/<int:name>/', views.create_comment, name='create_comment'),
    path('Edition_commentaire/<int:name>/', views.edit_comment, name='edit_comment'),
    path('liked/<int:name>/', views.like, name='like'),
    path('abonne/<int:name>/', views.abonne, name='abonne'),

    path("postule/refuse/<int:name>/", views.refu_post, name="refu_post"),
    path("postule/accept/<int:name>/", views.validation_post, name="validation_post"),

    path("associations/quit/<int:name>/", views.kick, name="kick"),

    path('search/', views.research, name="research"),
    path('user/<int:name>', views.show_user, name="show_user"),

    path('creation_association/', views.create_asso, name="create_asso"),
    path('creation_association/<str:name>', views.demand_asso, name="demand_asso"),
    path('creation_association/<str:name>', views.demand_asso, name="validate_asso"),
    path('creation_association/accept/<int:name>', views.validation_asso, name="yes_asso"),
    path('creation_association/refuse/<int:name>', views.refu_asso, name="no_asso"),

    path('import/association/', views.export_asso, name="export_asso"),
    path('import/member/', views.export_bureau, name="export_bureau"),
    path('import/enacs/', views.export_enacs, name="export_enacs"),

    path('reset_mdp/', views.edit_password, name="edit_password"),
    path('user/<int:name>', views.edit_password, name="change_pass"),
]
