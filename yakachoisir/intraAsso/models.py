from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import MaxValueValidator

class UserManager(BaseUserManager):
    def create_user(self, email, **extra_fields):
        user = self.model(
            email=email)
        user.save(using=self._db)
        return user

    def create_cri_user(self, firstname, lastname, email):
        user = self.model(
            first_name=firstname,
            last_name=lastname,
            email=email,
            cri=True)
        user.save(using=self._db)
        return user

    def create_staff(self, first_name, last_name, email, password):
        user = self.model(
            first_name=first_name,
            last_name=last_name,
            email=email)
        user.set_password(password)
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, first_name, last_name, email, password):
        user = self.model(
            first_name=first_name,
            last_name=last_name,
            email=email)
        user.set_password(password)
        user.admin = True
        user.staff = True
        user.save(using=self._db)
        return user

################################################################################

class User(AbstractBaseUser):
    first_name = models.CharField(max_length=64, null=True)
    last_name = models.CharField(max_length=64, null=True)
    email = models.EmailField(max_length=64, unique=True, null=False)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    cri = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"

    REQUIRED_FIELDS = ["first_name", "last_name"]

    def __str__(self):
        return self.first_name + " " + self.last_name

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    def has_perm(self, perm, obj=None):
        return self.admin

    def has_module_perms(self, app_label):
        return self.admin

    class Meta:
        unique_together = ('first_name', 'last_name')

################################################################################

class Association(models.Model):
    name = models.CharField(max_length=64, unique = True, null=False)
    mail = models.EmailField(max_length=64, unique = False, null=True)
    site = models.CharField(max_length=64, default='')
    description = models.TextField(default="")
    motivation = models.BooleanField(null=False, default=True)
    contract = models.FileField(default=None, upload_to='intraAsso/static/contract_association/')
    club = models.BooleanField(null=False, default=False)

    def __str__(self):
        return self.name

    def get_members(self):
        list = Member.objects.filter(association=self).exclude(role__role_str="None").exclude(role__role_str="President")
        return list

    def get_president(self):
        return Member.objects.get(association=self, role__role_str="President")

################################################################################

class Role(models.Model):
    role_str = models.CharField(max_length=64, unique = True, null=False)

    def __str__(self):
        return self.role_str

################################################################################

class File_type(models.Model):
    name = models.CharField(max_length=64, unique = False, null=False)

    def __str__(self):
        return self.name

################################################################################

class Imported_files(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    file_type = models.ForeignKey(File_type, on_delete=models.CASCADE)
    file = models.FileField(upload_to='intraAsso/static/imported/')

################################################################################

class Enacs_form(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    file = models.FileField(upload_to='intraAsso/static/enacs/')

    def __str__(self):
        return self.association

################################################################################

class Status(models.Model):
    status = models.CharField(max_length=64, unique = False, null=False)

    def __str__(self):
        return self.status

################################################################################

class Member(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    enacs = models.PositiveIntegerField(null=False, default=0)

    def __str__(self):
        return self.association.name + ", role : " + self.role.role_str

################################################################################

class Transaction(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    date = models.DateField(null=False)
    label = models.CharField(max_length=64, unique = False, null=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(null=False)
    justification_file = models.FileField(upload_to='intraAsso/static/transaction/')

################################################################################

class Reunion(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    date = models.DateField(null=False)
    place = models.CharField(max_length=64, unique = False, null=False)
    is_public = models.BooleanField(null=False)
    is_geneal = models.BooleanField(null=False)
    subject_file = models.FileField(upload_to='intraAsso/static/reunion_subject/')
    report_file = models.FileField(null=True, upload_to='intraAsso/static/reunion_report/')

################################################################################

class Enacs(models.Model):
    enabled = models.BooleanField(null=False)

################################################################################

class Budget_request(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    justification_file = models.FileField(upload_to='intraAsso/static/budget/')
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    date = models.DateField(null=False)
    amount = models.PositiveIntegerField(null=False)

################################################################################

class Rss_files(models.Model):
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    title = models.CharField(max_length=128, null=False)
    description = models.TextField(default="")

################################################################################

class Reaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reaction = models.ForeignKey(Rss_files, on_delete=models.CASCADE)
    commentaire = models.TextField(default="")

################################################################################

class Liked(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reaction = models.ForeignKey(Rss_files, on_delete=models.CASCADE)

################################################################################

class Rss_subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    association = models.ForeignKey(Association, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'association')

################################################################################

class Postulant(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    motiv_file = models.FileField(upload_to='intraAsso/static/motivation/', blank=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'association')

################################################################################

class Creation_association(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    name = models.CharField(max_length=64, default='Default',unique = True, null=False)
    mail = models.EmailField(max_length=64, unique = False, null=True)
    site = models.CharField(max_length=64, null=True, blank=True)
    description = models.TextField(default="")
    President = models.ForeignKey(User, on_delete=models.CASCADE, related_name='President')
    Vice_president = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Vice_president')
    Secretaire = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Secretaire')
    Trésorier = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Trésorier')
    Association_tutrice = models.ForeignKey(Association, on_delete=models.CASCADE)
    motivation = models.BooleanField(null=False, default=True)
    contract = models.FileField(default=None, upload_to='intraAsso/static/contract_association/')
    status = models.ForeignKey(Status, on_delete=models.CASCADE)

################################################################################

class Tuteur(models.Model):
    mother = models.ForeignKey(Association, on_delete=models.CASCADE)
    son = models.PositiveIntegerField(null=False) #id du club
