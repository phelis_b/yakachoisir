# YackaChoisir

## Pipenv Command

pipenv install --dev
pipenv shell

## Create database

- In order to use PostgreSQL, a database must be running
- The name of the database must be set in settings.py
- Current name for database is: intrasso
- Command are:

========================================================

echo 'export PGDATA="$HOME/postgres_data"' >> ~/.bashrc
echo 'export PGHOST="/tmp"' >> ~/.bashrc
source ~/.bashrc
initdb --locale "$LANG" -E UTF8
postgres -k "$PGHOST"

========================================================

- To create database do:

========================================================

psql postgres
postgres=# ALTER ROLE "<login>" SUPERUSER;
postgres=# CREATE DATABASE intrasso OWNER "<login>";
postgres=# \q

========================================================

## Migration

python manage.py makemigrations
python manage.py migrate

## Runserver

cd yakachoisir
python manage.py runserver

## Save database

- pg_dump -U intrasso > intrasso.pgsql

## Load database

- \i intrasso.pgsql (in the databse)


## delete migration

-python manage.py migrate --fake intraAsso
